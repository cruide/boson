<?php
@ini_set('date.timezone', 'Europe/Moscow');

define('BOSON_START_TIME', microtime(true));

define('DIR_SEP'    , DIRECTORY_SEPARATOR);
define('ROOT'       , __DIR__);
define('PROTOCOL'   , 'http' . ( (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') );
define('SELF_DOMAIN', $_SERVER['HTTP_HOST']);
define('BASE_URL'   , PROTOCOL . '://' . SELF_DOMAIN);

require('boson' . DIR_SEP . 'Bootstrap.php');
//require('boson.phar');