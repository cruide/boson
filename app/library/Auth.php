<?php namespace App\Library;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\Traits\SingletonTrait;
use App\Models\User;

final class Auth
{
    use SingletonTrait;
    
    private $autorization, $user;

    public function __construct()
    {
        $this->autorization = false;
        $token              = cookies()->token;

        $obj = User::where('session_id', '=', session()->id(true))
                   ->where('ip', '=', get_ip_address())
                   ->first();
        
        if( $obj ) {
            $obj->stamp = time();
            $obj->save();
            
            $this->autorization = true;
            $this->user         = $obj;
         
            unset($obj);
            
            if( !empty($token) && is_uuid($token) ) {
                cookies()->token = $token; 
            }
            
        } else if( !empty($token) && is_uuid($token) ) {
            $obj = User::where('token', '=', $token)
                       ->first();
            
            if( $obj ) {
                $obj->session_id = session()->id(true);
                $obj->stamp   = time();
                $obj->ip      = get_ip_address();
                $obj->save();
                
                cookies()->token = $token;
                
                $this->autorization = true;
                $this->user         = $obj;
             
                unset($obj);
            }
        }
    }

    public function signin($email, $password)
    {
        if( !is_email($email) ) {
            return false;
        }
        
        $user = User::where('email', '=', $email)
                    ->where('password', '=', password_crypt($password))
                    ->first();

        if( $user ) {
            $user->session_id = session()->id(true);
            $user->stamp   = time();
            $user->ip      = get_ip_address();
            $user->save();

            $this->autorization = true;
            $this->user         = $user;
            
            return true;
        }
        
        return false;
    }

    public function signout()
    {
        if( $this->autorization ) {
            $this->user->session_id = '';
            $this->user->stamp   = 0;
            $this->user->ip      = '';
            $this->user->save();

            $this->user         = null;
            $this->autorization = false;
            
            session()->destroy();
        }
    }

    public function user()
    {
        if( $this->autorization ) {
            return $this->user;
        }
        
        return null;
    }

    public function id()
    {
        if( $this->autorization ) {
            return $this->user->id;
        }
        
        return null;
    }

    public function check()
    {
        return $this->autorization;
    }
}
