<?php
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

router()->pattern('id', '[0-9]+');
router()->pattern('query', '[0-9a-z\.]+');

router()->get([
	['/', 'Index@index'],
	['/register', 'Index@register'],
	['/wot', 'Index@wot'],
	['/install', 'Install@index'],
]);
		
router()->any('/login', 'Index@login', 'index.login')
		->any('/logout', 'Index@logout', 'index.logout')
		->any('/register', 'Index@register', 'index.register');

router()->resource('/photo', 'Photo');
router()->get([
	['/passwords/update', 'Passwords@update'],
	['/passwords', 'Passwords@index'],
]);
