<?php return [

	'add'        => 'add',
	'and'        => 'and',
	'apply'      => 'apply',
	'attention'  => 'attention',
	'authorization' => 'authorization',

	'birthday'   => 'birthday',
	'block'      => 'block',

	'calendar'   => 'calendar',
	'cancel'     => 'cancel',
	'color'      => 'color',    
	'continue'   => 'continue',
	'close'      => 'close',
	'clear'      => 'clear',
	'comment'    => 'comment',
	'cloud'      => 'cloud',
	'code'       => 'code',
	'confirm'    => 'confirm',

	'date'       => 'date',
	'delete'     => 'delete',
	'document'   => 'document',
	'documents'  => 'documents',
	'description'=> 'description',    
	'download'   => 'download',
	
	'edit'       => 'edit',
	'enter'      => 'enter',
	'entry'      => 'entry',
	'error'      => 'error',
	'exit'       => 'exit',
	'email'      => 'email',

	'find'       => 'find',
	'filter'     => 'filter',
	'female'     => 'female',

	'gender'     => 'gender',
	
	'get'        => 'get',
	'group'      => 'group',
	'groups'     => 'groups',
	
	'home'       => 'home',
	
	'interfaces' => 'interfaces',
	'interface'  => 'interface',
	
	'kill'       => 'kill',

	'list'       => 'list',
	'language'   => 'language',
	'login'      => 'login',
	
	'more'       => 'more',
	'move'       => 'move',
	'message'    => 'message',
	'messages'   => 'messages',
	'member'     => 'member',
	'mobile'     => 'mobile',
	'male'       => 'male',
	
	'name'       => 'name',
	'nickname'   => 'nickname',
	'next'       => 'next',
	'note'       => 'note',
	'notes'      => 'notes',
	'no'         => 'no',
	'not'        => 'not',
	
	'ok'         => 'ok',
	'or'         => 'or',
	'open'       => 'open',
	'option'     => 'option',
	'options'    => 'options',
	'other'      => 'other',

	'previous'   => 'previous',
	'personal'   => 'personal',
	'profile'    => 'profile',
	'password'   => 'password',
	'page'       => 'pages',
	'phone'      => 'phone',

	'remove'     => 'remove',
	'role'       => 'role',
	'reset'      => 'reset',
	'registration' => 'registration',
	
	'settings'   => 'settings',
	'search'     => 'search',
	'statistic'  => 'statistic',
	'save'       => 'save',
	'send'       => 'send',

	'task'       => 'task',
	'tasks'      => 'tasks',
	'time'       => 'time',
	'today'      => 'today',
	'type'       => 'type',
	'tree'       => 'tree',

	'user'       => 'user',
	'users'      => 'users',
	'update'     => 'update',
	'upload'     => 'upload',

	'yes'        => 'yes',
	'you'        => 'you',
	'your'       => 'your',
	
	'warning'    => 'warning',
	'window'     => 'window',
	'windows'    => 'windows',
	
	'year'       => 'year',
	'month'      => 'month',
	'week'       => 'week',
	'day'        => 'day',
	
	'january'   => 'january', 
	'february'  => 'february', 
	'march'     => 'march', 
	'april'     => 'april', 
	'may'       => 'may', 
	'june'      => 'june', 
	'july'      => 'july', 
	'august'    => 'august', 
	'september' => 'september', 
	'october'   => 'october', 
	'november'  => 'november', 
	'december'  => 'december',
	
	'monday'    => 'monday', 
	'tuesday'   => 'tuesday', 
	'wednesday' => 'wednesday', 
	'thursday'  => 'thursday', 
	'friday'    => 'friday', 
	'saturday'  => 'saturday',
	'sunday'    => 'sunday',
	
	'validator_required' => 'required to fill',
	'validator_minlen'   => 'must be greater than :maxlen characters',
	'validator_maxlen'   => 'should not exceed :maxlen characters',
	'validator_min'      => 'should not be less than :min',
	'validator_max'      => 'should not be more than :max',
	'validator_numeric'  => 'must be a number',
	'validator_regexp'   => 'does not meet the required format',
];