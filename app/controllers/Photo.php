<?php namespace App\Controllers;

use Boson\Interfaces\Resource;

class Photo implements Resource
{
    public function index()
    {
        return json_response([
            'user' => auth()->user(),
        ]);
    }
    
    public function edit($id = null)
    {
        
    }
}
