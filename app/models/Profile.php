<?php namespace App\Models;

class Profile extends \Boson\Abstracts\EloquentModel
{
    protected $table    = 'user_profiles';
    protected $fillable = [
        'gender',
        'first_name',
        'middle_name',
        'last_name',
        'birthday',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}