<?php namespace App\Models;

class User extends \Boson\Abstracts\EloquentModel
{
    protected $table    = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }
}