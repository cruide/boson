<?php

function auth()
{
    return \App\Library\Auth::getInstance();
}

function is_auth()
{
    return auth()->check();
} 
