<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\Traits\SingletonTrait;
use Boson\Abstracts\Registry;

final class Input extends Registry
{
    use SingletonTrait;
    
    protected $bearer;

    public function __construct()
    {
        global $_GET, $_POST, $_SERVER;

        $this->properties['headers'] = [];
        
        if( isset($_GET) && array_count($_GET) > 0 ) {
            foreach($_GET as $key=>$val) {
                $this->properties[ $this->_clean_key($key) ] = $this->_clean_val($val);
            }
        }

        if( isset($_POST) && array_count($_POST) > 0 ) {
            foreach($_POST as $key=>$val) {
                $this->properties[ $this->_clean_key($key) ] = $this->_clean_val($val);
            }
        }
        
        $this->getBearerToken();
    }

    public function all()
    {
        return $this->toArray();
    }
    
    public function getBearerToken()
    {
        global $_SERVER;

        if( $this->bearer !== null ) {
            return $this->bearer;
        }
        
        $_header = null;
        
        if( array_key_isset('Authorization', $_SERVER) ) {
            $_header = trim( $_SERVER['Authorization'] );
            
        } else if( array_key_isset('HTTP_AUTHORIZATION', $_SERVER) ) {
            $_header = trim( $_SERVER['HTTP_AUTHORIZATION'] );
            
        } else if( function_exists('apache_request_headers') ) {
            $apache_request_headers = apache_request_headers();
            $apache_request_headers = array_combine(
                array_map('ucwords', array_keys($apache_request_headers)), array_values($apache_request_headers)
            );
            
            if( array_key_isset('Authorization', $apache_request_headers) ) {
                $_header = trim( $apache_request_headers['Authorization'] );
            }
        }

        if( !empty($_header) && preg_match('/Bearer\s+(.*)/', $_header, $matches) ) {
            $this->bearer = !empty($matches[1]) ? $matches[1] : false;
            
        } else {
            $this->bearer = false;
        }
        
        return $this->bearer;
    }
    
    protected function getClean( $name = null )
    {
        return array_key_isset($name, $this->properties) ? $this->_xss_clean( $this->properties[$name] ) : null;
    }

    protected function _clean_key($str)
    {
        if( !preg_match("/^[a-z0-9:_\\/-]+$/i", $str) ) {
            abort("Your request {$str} contains disallowed characters.");
        }

        return $str;
    }

    protected function _clean_val($str)
    {
        if( is_array($str) ) {
            $_array = [];

            foreach($str as $key=>$val) {
                $_array[ $this->_clean_key($key) ] = $this->_clean_val($val);
            }

            return $_array;
        }

        return preg_replace("/\015\012|\015|\012/", "\n", $str);
    }

    protected function _xss_clean($data)
    {
        if( is_array($data) ) {
            $_ = [];

            foreach($data as $key=>$val) {
                $_[ $key ] = $this->_xss_clean($val);
            }

            return $_;
        }

        // Fix &entity\n;
        $data = str_replace(['&amp;','&lt;','&gt;'], ['&amp;amp;','&amp;lt;','&amp;gt;'], (string)$data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" || xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: && vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        } while( $old_data !== $data );

        // we are done...
        return $data;
    }
}

