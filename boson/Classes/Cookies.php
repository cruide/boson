<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\Traits\SingletonTrait;

class Cookies
{
    use SingletonTrait;

    protected $_properties = [];

    public function __construct()
    {
        global $_COOKIE;

        if( !empty($_COOKIE) ) {
            foreach($_COOKIE as $key=>$val) {
                if( $key != 'PHPSESSID' ) {
                    $this->_properties[ $key ] = $val;
                }
            }
        }
    }

    /**
    * Cookie getter for $this->_properties
    *
    * @param strung $name
    */
    public function __get($name)
    {
        if( isset($this->_properties[$name]) && array_key_exists($name, $this->_properties) ) {
            return $this->_properties[ $name ];
        }

        return null;
    }

    /**
    * Cookie setter for $this->_properties
    *
    * @param string $name
    * @param mixed $value
    */
    public function __set($name, $value)
    {
        if( array_key_isset($name, $this->_properties) && $value == null ) {
            return $this->__unset($name);
        }
        
        setcookie($name, $value, time() + (86400 * 7), '/');
        $this->_properties[ $name ] = $value;
    }

    /**
    * Cookie isset for $this->_properties
    *
    * @param string $name
    * @return bool
    */
    public function __isset($name)
    {
        return isset($this->_properties[$name]);
    }

    /**
    * Cookie unset for $this->_properties
    *
    * @param string $name
    */
    public function __unset($name)
    {
        setcookie($name, '', time() - 3600, '/');
        unset($this->_properties[$name]);
    }
}


