<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

class Str
{
    public static function ucfirst($str)
    {
        if( function_exists('mb_strlen') ) {
            return mb_strtoupper( mb_substr( $str, 0, 1, 'UTF-8' ), 'UTF-8' ) . mb_substr( $str, 1, mb_strlen( $str ), 'UTF-8' );
        }

        if( function_exists('iconv') ) {
            return iconv('windows-1251', 'utf-8', ucfirst( iconv('utf-8', 'windows-1251', $str) ) );
        }

        return ucfirst( $str );
    }
    
    public static function lower()
    {
        if( function_exists('mb_strtolower') ) {
            return mb_strtolower($str, 'UTF-8');
        }
        
        if( function_exists('iconv') ) {
            return iconv('windows-1251', 'utf-8', strtolower( iconv('utf-8', 'windows-1251', $str) ) );
        }

        return strtolower( $str );
    }

    public static function upper()
    {
        if( function_exists('mb_strtoupper') ) {
            return mb_strtoupper($str, 'UTF-8');
        }
        
        if( function_exists('iconv') ) {
            return iconv('windows-1251', 'utf-8', strtoupper( iconv('utf-8', 'windows-1251', $str) ) );
        }

        return strtoupper( $str );
    }

    public static function length($str)
    {
        if( function_exists('mb_strlen') ) {
            return mb_strlen($str, 'UTF-8');
        }

        if( function_exists('iconv') ) {
            return strlen( iconv('utf-8', 'windows-1251', $str) );
        }

        return strlen( $str );
    }
    
    public static function strstr($haystack, $needle, $part = false)
    {
        return ( function_exists('mb_strstr') ) ? mb_strstr($haystack, $needle, $part, 'UTF-8') : strstr($haystack, $needle, $part);
    }
    
    public static function crop($string)
    {
        $string = strip_tags($string);

        if( function_exists('mb_strlen') ) {
            $len    = (mb_strlen($string) > $lenght) ? mb_strripos( mb_substr($string, 0, $lenght), ' ' ) : $lenght;
            $result = mb_substr($string, 0, $len);

            return (mb_strlen($string) > $lenght) ? $result . '...' : $result;
        }

        if( function_exists('iconv') ) {
            $result = iconv('utf-8', 'windows-1251', $string);
            $length = strripos( substr($result, 0, $length), ' ');

            return iconv('windows-1251', 'utf-8', substr($result, 0, $length) );
        }

        $len    = (strlen($string) > $lenght) ? strripos( substr($string, 0, $lenght), ' ' ) : $lenght;
        $result = substr($string, 0, $len);

        return (mb_strlen($string) > $lenght) ? $result . '...' : $result;
    }

    public static function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
    {
        if ($length == 0) {
            return '';
        }

        if( function_exists('mb_substr') ) {
            if( mb_strlen($string, 'UTF-8') > $length ) {
                $length -= min($length, mb_strlen($etc, 'UTF-8'));

                if( !$break_words && !$middle ) {
                    $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length + 1, 'UTF-8'));
                }

                if( !$middle ) {
                    return mb_substr($string, 0, $length, 'UTF-8') . $etc;
                }

                return mb_substr($string, 0, $length / 2, 'UTF-8') . $etc . mb_substr($string, - $length / 2, $length, 'UTF-8');
            }

           return $string;
       }

       // no MBString fallback
       if( isset($string[ $length ]) ) {
           $length -= min($length, strlen($etc));

           if( !$break_words && !$middle ) {
               $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
           }
           if( !$middle ) {
               return substr($string, 0, $length) . $etc;
           }

           return substr($string, 0, $length / 2) . $etc . substr($string, - $length / 2);
       }

       return $string;
    }
}