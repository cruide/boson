<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2017 All rights reserved
*/

  class TempException extends \Exception {}  

  class Temp
  {
      use \Boson\Traits\ClassName;
      
      protected $_name    = '';
      protected $_dir     = '';
      protected $_content = '';
      protected $_encrypt = false;

      /**
      * Constructor
      * 
      * @param string $name
      * @return Temp
      */
      public function __construct( $name, $directory = null )
      {
          if( empty($name) ) {
              throw new TempException( 
                  $this->className() . '::__construct - Need temp name' 
              );
          }
          
          $this->_dir  = ( !empty($directory) && is_dir($directory) ) 
                             ? path_correct($directory, true) 
                             : path_correct(TEMP_DIR, true);
                             
          $this->_name = $name;
      }

      public function encryption()
      {
          $this->_encrypt = true;
          
          return $this;
      }
      
      public function path( $dir )
      {
          if( is_dir($dir) ) {
              $this->_dir = path_correct($dir, true);
          }
          
          return $this;
      }

      /**
      * Set temp content
      * 
      * @param mixed $content
      * @return Temp
      */
      public function content( $content = null )
      {
          if( is_scalar($content) ) {
              $this->_content = $content;
          }
          
          return $this;
      }

      /**
      * Write serialized content to file
      * 
      */
      public function write()
      {
          $data = $this->_encrypt ? encrypt( serialize($this->_content) ) 
                                  : serialize($this->_content);
          
          if( !file_put_gz_content( $this->_dir . $this->_name, $data ) ) {
              throw new TempException( 
                  $this->className() . '::write - Could not write a temporary file. Check the correctness of the path.' 
              );
              
              return false;
          }
          
          return true;
      }

      /**
      * Read data from temp
      */
      public function read()
      {
		  if( is_file($this->_dir . $this->_name) && is_readable($this->_dir . $this->_name) ) {
			  $this->_content = $this->_encrypt ? unserialize( decrypt( file_get_gz_content($this->_dir . $this->_name) ) )
                                                : unserialize( file_get_gz_content($this->_dir . $this->_name) );
		  }
		  
		  return $this->_content;
      }

      /**
      * Delete temp file
      */
      public function delete()
      {
          if( is_file($this->_dir . $this->_name) ) {
              @unlink($this->_dir . $this->_name);
          }
      }
      
      public static function create($filename, $content)
      {
          $temp = new self($filename);
          $temp->content($content);
          $temp->write();
          
          return $temp;
      }
      
      public static function pull($filename)
      {
          $temp = new self($filename);
          $data = $temp->read();
          
          $temp->delete();
          
          return $data;
      }
  }
