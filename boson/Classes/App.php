<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\Traits\SingletonTrait;
use Boson\Native;

class AppException extends \Exception {}

class App
{
    use SingletonTrait;

    protected $input;
    protected $router;
    protected $cfg;
    protected $controller;
    protected $layout;
    protected $layout_disabled;
    protected $theme;

    public function __construct()
    {
        $this->router = router();
        $this->input  = input();
        $this->theme  = theme();

        if( !($routing = $this->router->getRoute()) ) {
            abort('Not found...', 404);
        }
        
        session();
        cookies();

        $className = '\\App\\Controllers\\' . $routing['controller'];

        try {
            $this->controller = new $className();
            
        } catch( AppException $e ) {
            abort( $e->getMessage() . "\n" . $e->getFile() . "\n" . $e->getLine() );
        }
    }

    public function getController()
    {
        return $this->controller;
    }
    
    public function execute()
    {
        global $_SERVER;

        $method = $this->router->getMethodName();
        $params = $this->router->getParams();

        $this->theme->setGlobals();
        
        if( method_exists($this->controller, '_before') ) {
            $this->controller->_before();
        }
        
        try {
            $content = (array_count($params) > 0)
                           ? call_user_func_array([$this->controller, $method], $params)
                           : call_user_func([$this->controller, $method]);  
        
                           
        } catch( AppException $e ) {
            abort( $e->getMessage() );
        }

        if( method_exists($this->controller, '_after') ) {
            $this->controller->_after();
        }

        try {
            $this->theme->display( $content );
            
        } catch( AppException $e ) {
            abort( $e->getMessage() );
        }
    }
}
