<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\BosonObject;

final class I18n
{
    use \Boson\Traits\SingletonTrait;
        
    private $strings;
    private $lagnuages = [
        'en' => 'English',
        'ru' => 'Русский',
        'ua' => 'Український',
        'be' => 'Беларускі',
        'de' => 'Deutsch',
        'fr' => 'Français',
    ];
    
    private $default = 'en';
    private $current = 'en';
    
// -----------------------------------------------------------------------------
    public function __construct()
    {
        /**
        * Просматриваем какие языки есть
        */
        foreach($this->lagnuages as $key=>$val) {
            $langfile = $key . '.php';
            
            if( !is_file(LANG_DIR . DIR_SEP . $langfile) ) {
                unset($this->lagnuages[ $key ]);
            }
        }
        
        unset($langfile, $key, $val);
        
        $cookie_lang = cookies()->lang;
        $config_lang = cfg('config', 'lang');
        
        if( !empty($cookie_lang) && array_key_isset($cookie_lang, $this->lagnuages) ) {
            $this->current = $cookie_lang;
            
        } else if( !empty($config_lang) && array_key_isset($config_lang, $this->lagnuages) ) {
            $this->current = $config_lang;
        }
        
        if( empty($this->lagnuages) ) {
            return;
        }
        
        /**
        * Загружаем языковые пакеты
        */
        $this->strings     = new BosonObject();
        $default_lang_file = $this->default . '.php';

        if( is_file(LANG_DIR . DIR_SEP . $default_lang_file) ) {
            $default_lng_strings = include(LANG_DIR . DIR_SEP . $default_lang_file); 
            
            if( array_count($default_lng_strings) > 0 ) {
                foreach($default_lng_strings as $key=>$val) {
                    if( is_varible_name($key) && is_scalar($val) ) {
                        $this->strings->$key = $val;
                    }
                }
            }
        }
        
        if( $this->default != $this->current ) {
            $default_lang_file  = $this->current . '.php';
            
            if( is_file(LANG_DIR . DIR_SEP . $default_lang_file) ) {
                $default_lng_strings = include(LANG_DIR . DIR_SEP . $default_lang_file); 
                
                if( array_count($default_lng_strings) > 0 ) {
                    foreach($default_lng_strings as $key=>$val) {
                        if( is_varible_name($key) && is_scalar($val) ) {
                            $this->strings->$key = $val;
                        }
                    }
                }
            }
        }
    }
// -----------------------------------------------------------------------------
    /**
    * Получение кода языка, 
    * установленного по умолчанию
    */
    public function getDefaultLang()
    {
        return $this->default;
    }
// -----------------------------------------------------------------------------
    /**
    * Получение текущего языка
    */
    public function getCurrentLang()
    {
        return $this->current;
    }
// -----------------------------------------------------------------------------
    /**
    * Установка текущего языка
    */
    public function setCurrentLang( $lang_id )
    {
        $lang_id = strtolower( $lang_id );
        
        if( array_key_isset($lang_id, $this->lagnuages) ) {
            cookies()->lang = $lang_id;
        }
        
        return $this;
    }
// -----------------------------------------------------------------------------
    /**
    * Получение списка поддерживаемых языков
    */
    public function getLanguages()
    {
        return $this->lagnuages;
    }
// -----------------------------------------------------------------------------
    public function has( $key )
    {
        return ( !empty($key) && isset($this->strings->$key) );
    }
// -----------------------------------------------------------------------------
    public function get( $key, array $values = [] )
    {
        if( $this->has($key) ) {
            $_ = $this->strings->$key;
            
            if( array_count($values) > 0 ) {
                foreach($values as $key=>$val) {
                    if( is_scalar($val) ) {
                        $_ = str_replace(":{$key}", $val, $_);
                    }
                }
            }
            
            return $_;
        }
        
        return "::{$key}::";
    }
}

