<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

final class Theme
{
    use \Boson\Traits\SingletonTrait;
    
    protected $_css_list;
    protected $_js_list;
    protected $_theme_name;
    protected $_module_name;
    protected $_layout;
    protected $_config;
    protected $render;
    protected $_headers;
    protected $_cover;
    
    protected $view;
    protected $layout;
    
// -------------------------------------------------------------------------------------
    public function __construct()
    {
      	$this->_config     = cfg('config');
        $this->_theme_name = $this->_config->theme;
        $this->_layout     = $this->_config->layout;
        $this->_cover      = $this->_config->cover;
        $this->_headers    = [];

        if( empty($this->_theme_name) ) {
            $this->_theme_name = 'default';
        }

        if( empty($this->_layout) ) {
            $this->_layout = 'layout';
        }

        $this->setHeader( CONTENT_TYPE_HTML );
        $this->enableLayout();

        if( $this->_cover == 'smarty' && class_exists('\Smarty') ) {

            if( !preg_match('#\.tpl$#', $this->_layout) ) {
                $this->_layout .= '.tpl';
            }
        
            $this->layout = new \Smarty();
            $this->view   = new \Smarty();
            
            $this->layout->setTemplateDir( THEMES_DIR . DIR_SEP . $this->_theme_name . DIR_SEP . 'views' );
            $this->layout->setCompileDir( SMARTY_TEMP_DIR );
            $this->layout->setCacheDir( SMARTY_TEMP_DIR . DIR_SEP . 'cache' );
            
            $this->view->setTemplateDir( THEMES_DIR . DIR_SEP . $this->_theme_name . DIR_SEP . 'views' );
            $this->view->setCompileDir( SMARTY_TEMP_DIR );
            $this->view->setCacheDir( SMARTY_TEMP_DIR . DIR_SEP . 'cache' );
            
        } else {

            $this->layout = new Native( THEMES_DIR . DIR_SEP . $this->_theme_name . DIR_SEP . 'views' . DIR_SEP );
            $this->view   = new Native( THEMES_DIR . DIR_SEP . $this->_theme_name . DIR_SEP . 'views' . DIR_SEP );
        }
    }
// -------------------------------------------------------------------------------------
    public function layout()
    {
        return $this->layout;
    }
// -------------------------------------------------------------------------------------
    public function view()
    {
        return $this->view;
    }
// -------------------------------------------------------------------------------------
    /**
    * Set headers
    *
    * @param string $header
    * @return Theme
    */
    public function setHeader($header)
    {
        /* if $header is array */
        if( array_count($header) > 0 ) {
            foreach($header as $val) {
                if( !empty($val) && is_string($val) && !in_array($val, $this->_headers) ) {
                    if( preg_match('/^Content\-type/i', $val) ) {
                        $this->_contentTypeClear();
                    }

                    $this->_headers[] = $val;
                }
            }

            return $this;
        }

        /* if $header is string */
        if( !empty($header) && is_string($header) && !in_array($header, $this->_headers) ) {
            if( preg_match('/^Content\-type/i', $header) ) {
                $this->_contentTypeClear();
            }

            $this->_headers[] = $header;
        }

        return $this;
    }
// -------------------------------------------------------------------------------------
    protected function _contentTypeClear()
    {
        $tmp = [];

        foreach($this->_headers as $val) {
            if( !preg_match('/^Content\-type/i', $val) ) {
                $tmp[] = $val;
            }
        }

        $this->_headers = $tmp;
    }
// -------------------------------------------------------------------------------------
    public function setLayoutName($name)
    {
        if( !preg_match('#\.tpl$#', $name) ) {
            $name = $name . '.tpl';
        }

        if( !empty($name) && is_file($this->getThemePath() . DIR_SEP . $name) ) {
            $this->_layout = $name;
        }
    }
// -------------------------------------------------------------------------------------
    public function useExternalCss($url)
    {
        if( !is_array($this->_css_list) ) {
            $this->_css_list = [];
        }

        if( is_url_exists($url) ) {
            $this->_css_list[] = $url;
        }

        return $this;
    }
// -------------------------------------------------------------------------------------
    public function useThemeCss($css_filename)
    {
        $css_file_path = path_correct( $this->getThemePath() . DIR_SEP . 'css' . DIR_SEP . $css_filename );

        if( is_file($css_file_path) ) {
            $this->_css_list[] = $css_filename;
        }

        return $this;
    }
// -------------------------------------------------------------------------------------
    /**
    * Добавление внешнего JS скрипта
    *
    * @param string $url
    * @param bool $head - в заголовке <HEAD> или перед </BODY>
    * @return Theme
    */
    public function useExternalJs($url, $head = true)
    {
        if( !is_array($this->_js_list) ) {
            $this->_js_list = [];
        }

        $position = ($head === false) ? 'footer' : 'header';

        if( !is_array($this->_js_list[ $position ]) ) {
            $this->_js_list[ $position ] = [];
        }

        if( is_url_exists($url) ) {
            $this->_js_list[ $position ][] = $url;
        }

        return $this;
    }
// -------------------------------------------------------------------------------------
    /**
    * Добавление JS скрипта
    *
    * @param string $js_filename
    * @param bool $head - в заголовке <HEAD> или перед </BODY>
    * @return Theme
    */
    public function useThemeJs($js_filename, $head = true)
    {
        $js_file_path = path_correct( $this->getThemePath() . DIR_SEP . 'js' . DIR_SEP . $js_filename );

        if( is_file($js_file_path) ) {
            $position = ($head === false) ? 'body' : 'head';

            if( !is_array($this->_js_list[ $position ]) ) {
                $this->_js_list[ $position ] = [];
            }

            $this->_js_list[ $position ][] = $js_filename;
        }

        return $this;
    }
// -------------------------------------------------------------------------------------
    public function setTheme($theme_name)
    {
        if( !empty($theme_name) && is_dir(THEMES_DIR . DIR_SEP . $theme_name) ) {
            $this->layout->setTemplateDir( THEMES_DIR . DIR_SEP . $theme_name . DIR_SEP . 'views' . DIR_SEP );
            $this->view->setTemplateDir( THEMES_DIR . DIR_SEP . $theme_name . DIR_SEP . 'views' . DIR_SEP );
        }
        
        return $this;
    }
// -------------------------------------------------------------------------------------
    public function disableLayout()
    {
		$this->render = false;
		return $this;
    }
// -------------------------------------------------------------------------------------
    public function enableLayout()
    {
		$this->render = true;
		return $this;
    }
// -------------------------------------------------------------------------------------
    public function getThemeUrl()
    {
        return THEMES_URL . '/' . $this->_theme_name;
    }
// -------------------------------------------------------------------------------------
    public function getThemeName()
    {
        return $this->_theme_name;
    }
// -------------------------------------------------------------------------------------
    public function getThemePath()
    {
        return THEMES_DIR . DIR_SEP . $this->_theme_name;
    }
    
    public function getThemeViewsPath()
    {
        return THEMES_DIR . DIR_SEP . $this->_theme_name . DIR_SEP . 'views';
    }
// -------------------------------------------------------------------------------------
    public function setGlobals()
    {
        $this->layout->assignGlobal('base_url'   , BASE_URL);
        $this->layout->assignGlobal('content_url', CONTENT_URL);
        $this->layout->assignGlobal('js_url'     , THEMES_URL . "/{$this->_theme_name}/js");
        $this->layout->assignGlobal('css_url'    , THEMES_URL . "/{$this->_theme_name}/css");
        $this->layout->assignGlobal('images_url' , THEMES_URL . "/{$this->_theme_name}/images");
        
        return $this;
    }
// -------------------------------------------------------------------------------------
    public function display( $content )
    {
        $this->setGlobals();
        
        if( session()->has('redirect_message') ) {
            $this->layout->assignGlobal('redirect_message', session()->redirect_message);
            session()->redirect_message = null;
        }

        if( session()->has('redirect_error') ) {
            $this->layout->assignGlobal('redirect_error', session()->redirect_error);
            session()->redirect_error = null;
        }

        if( !headers_sent() && array_count($this->_headers) > 0 ) {
            foreach($this->_headers as $key=>$val) {
                header($val);
            }
        }

        if( !$this->render ) {
            http_cache_off();
            send_header_app_info();

            echo $content;
            
			return;
        }

        $out = $this->layout
                    ->assign('content', $content)
                    ->fetch( $this->_layout );

        memory_clear();

        if( str_length($out) > 102400 ) {
            @ini_set('zlib.output_compression', 1);
        }
        
        /**
        * Add CSS
        */
        if( array_count($this->_css_list) > 0 ) {
            $_ = "\n\t\t<!-- BOSON DYNAMIC CSS -->\n";

            foreach($this->_css_list as $key=>$val) {
                if( preg_match('/^http/is', $val) ) {
                    $_ .= "\t\t<link href=\"{$val}\" rel=\"stylesheet\" type=\"text/css\" />\n";
                } else {
                    $url = $this->getThemeUrl() . '/css/' . $val;
                    $_ .= "\t\t<link href=\"{$url}\" rel=\"stylesheet\" type=\"text/css\" />\n";
                }
            }

            $out = preg_replace('#\<\/head\>#is', $_ . "</head>\n", $out);

            unset($_, $key, $val, $url);
        }

        /**
        * Add JS
        */
        if( array_count($this->_js_list) > 0 ) {
            foreach($this->_js_list as $pos=>$item) {
                $_ = str_replace(':position', str_upper($pos), "\n\t\t<!-- :position BOSON DYNAMIC JS -->\n");

                if( array_count($item) > 0 ) {
                    foreach($item as $key=>$val) {
                        if( preg_match('/^http/is', $val) ) {
                            $_ .= "\t\t<script type=\"text/javascript\" src=\"{$val}\"></script>\n";
                        } else {
                            $url = $this->getThemeUrl() . '/js/' . $val;
                            $_ .= "\t\t<script type=\"text/javascript\" src=\"{$url}\"></script>\n";
                        }
                    }

                    $out = preg_replace("#\<\/{$pos}\>#is", $_ . "</{$pos}>\n", $out);
                    unset($_, $key, $val, $url);
                }
            }

            unset($pos, $item);
        }
        
        send_header_app_info();
        
        echo $out;
    }
}
