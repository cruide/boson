<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\Traits\SingletonTrait;

class MicroRouterException extends \Exception {}

class MicroRouter
{
    use SingletonTrait;
    
    protected $_request       = null;
    protected $_routes        = [];
    protected $_current_route = null;
    protected $_patterns      = [];
    protected $_request_types = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'PATCH',
        'ANY',
    ];

    protected $superfluous    = [
        '\.html', 
        '\.htm', 
        '\.php5', 
        '\.php', 
        '\.php3', 
        '\.shtml', 
        '\.phtml', 
        '\.dhtml', 
        '\.xhtml', 
        '\.inc', 
        '\.cgi', 
        '\.pl',
        '\.xml', 
        '\.js',
    ];
    
    public function __construct()
    {
        global $_SERVER;

        $REQUEST        = explode('?', $_SERVER['REQUEST_URI']);
        $this->_request = preg_replace('/\/+/', '/', $REQUEST[0]);
        
        if( $this->_request != '/' ) {
            $this->_request = preg_replace('/\/$/', '', $this->_request);
        }
        
        $superfluous    = implode('|', $this->superfluous);
        $this->_request = preg_replace("/({$superfluous})$/i", '', $this->_request);
    }
    
    public function getRoute()
    {
        global $_SERVER;

        if( !empty($this->_current_route) ) {
            return $this->_current_route;
        }

        $type = $_SERVER['REQUEST_METHOD'];

        foreach($this->_routes as $route) {
            if( ($route['type'] == $type || $route['type'] == 'ANY') && preg_match("#{$route['regexp']}#", $this->_request, $tmp) ) {
                $this->_current_route           = $route;
                $this->_current_route['params'] = [];
                
                unset($tmp[0]);
                
                foreach($tmp as $val) {
                    $this->_current_route['params'][] = $val;
                }
                
                return $this->_current_route;
            }
        }
        
        return null;
    }
    
    public function getAllRoutes()
    {
        return $this->_routes;
    }
    
    public function isRouteNameExists($name)
    {
        foreach($this->_routes as $route) {
            if( !empty($route['name']) && $route['name'] == $name ) {
                return true;
            }
        }
        
        return false;
    }
    
    public function getControllerName()
    {
        $this->getRoute();
        
        return $this->_current_route['controller'];
    }

    public function getMethodName()
    {
        $this->getRoute();
        
        return $this->_current_route['method'];
    }
    
    public function getParams()
    {
        $this->getRoute();
        
        return $this->_current_route['params'];
    }

    public function get($path, $data = null)
    {
        return $this->set('GET', $path, $data);
    }
    
    public function post($path, $data = null)
    {
        return $this->set('POST', $path, $data);
    }
    
    public function any($path, $data = null)
    {
        return $this->set('ANY', $path, $data);
    }
    
    public function put($path, $data = null)
    {
        return $this->set('PUT', $path, $data);
    }
    
    public function patch($path, $data = null)
    {
        return $this->set('PATCH', $path, $data);
    }
    
    public function delete($path, $data = null)
    {
        return $this->set('DELETE', $path, $data);
    }
    
    public function resource($path, $controller)
    {
        $className = '\\App\\Controllers\\' . $controller;
        
        if( !controller_exists($controller) ) {
            throw new MicroRouterException(
                "The specified controller {$controller}.php does not exist"
            );
        }
        
        if( !class_exists($className) ) {
            require(ACTIONS_DIR . DIR_SEP . "{$controller}.php");
            
            if( !class_exists($className) ) {
                throw new MicroRouterException(
                    'Incorrect comtroller class name'
                );
            }
        }

        $obj = new $className();

        if(  !is_subclass_of($obj, '\Boson\Interfaces\Resource') ) {
            throw new MicroRouterException(
                'The specified controller is not a resource'
            );
        }
        
        $path = preg_replace('#^\/#', '', $path);
        $path = preg_replace('#\/$#', '', $path);
        
        $controller_name = snake_case($controller);
        
        if( method_exists($obj, 'index') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'index',
                'name'       => $controller_name . '.index',
                'path'       => "{$path}",
                'regexp'     => "^\/{$path}$",
                'type'       => 'GET',
            ];
        }
        
        if( method_exists($obj, 'create') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'create',
                'name'       => $controller_name . '.create',
                'path'       => "{$path}/create",
                'regexp'     => "^\/{$path}\/create$",
                'type'       => 'GET',
            ];
        }
        
        if( method_exists($obj, 'show') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'show',
                'name'       => $controller_name . '.show',
                'path'       => "{$path}/{id}",
                'regexp'     => "^\/{$path}\/([0-9]+)$",
                'type'       => 'GET',
            ];
        }
        
        if( method_exists($obj, 'edit') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'edit',
                'name'       => $controller_name . '.edit',
                'path'       => "{$path}/{id}/edit",
                'regexp'     => "^\/{$path}\/([0-9]+)\/edit$",
                'type'       => 'GET',
            ];
        }

        if( method_exists($obj, 'store') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'store',
                'name'       => $controller_name . '.store',
                'path'       => "{$path}",
                'regexp'     => "^\/{$path}$",
                'type'       => 'POST',
            ];
        }

        if( method_exists($obj, 'update') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'update',
                'name'       => $controller_name . '.update',
                'path'       => "{$path}/{id}/update",
                'regexp'     => "^\/{$path}\/([0-9]+)\/update$",
                'type'       => 'POST',
            ];
        }

        if( method_exists($obj, 'destroy') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'destroy',
                'name'       => $controller_name . '.destroy',
                'path'       => "{$path}/{id}/destroy",
                'regexp'     => "^\/{$path}\/([0-9]+)\/destroy$",
                'type'       => 'POST',
            ];
        }

        if( method_exists($obj, 'update') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'update',
                'name'       => $controller_name . '.update.put',
                'path'       => "{$path}/{id}",
                'regexp'     => "^\/{$path}\/([0-9]+)$",
                'type'       => 'PUT',
            ];
        }

        if( method_exists($obj, 'update') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'update',
                'name'       => $controller_name . '.update.patch',
                'path'       => "{$path}/{id}",
                'regexp'     => "^\/{$path}\/([0-9]+)$",
                'type'       => 'PATCH',
            ];
        }
        
        if( method_exists($obj, 'destroy') ) {
            $this->_routes[] = [
                'controller' => $controller,
                'method'     => 'destroy',
                'name'       => $controller_name . '.destroy.delete',
                'path'       => "{$path}/{id}",
                'regexp'     => "^\/{$path}\/([0-9]+)$",
                'type'       => 'DELETE',
            ];
        }

        return $this;
    }
    
    protected function set($type, $path, $data = null, $route_name = null)
    {
        $type = strtoupper($type);
        
        if( !in_array($type, $this->_request_types) ) {
            throw new MicroRouterException(
                "Invalid request type specified"
            );
        }
        
        if( is_array($path) ) {
            foreach($path as $item) {
                if( empty($item[0]) || empty($item[1]) ) {
                    throw new MicroRouterException(
                        "Incorrect routing array data " . json_encode($item)
                    );
                }
                
                $this->set($type, $item[0], $item[1], (!empty($item[2]) ? $item[2] : null));
            }
            
            return $this;
        }
        
        if( empty($data) ) {
            throw new MicroRouterException(
                "No controller and method for route '{$path}' specified"
            );
        }

        if( is_scalar($data) ) {
            list($controller, $method) = explode('@', $data);
            
            $controller_name = snake_case($controller);
            $method_name     = snake_case($method);
            
            $data = [
                'controller' => $controller,
                'method'     => $method,
            ];
            
            $data['name'] = (!empty($route_name) && is_scalar($route_name)) 
                                ? $route_name
                                : $controller_name . '.' . $method_name;
        }
        
        if( empty($data['controller']) || empty($data['method']) ) {
            throw new MicroRouterException(
                "No controller and method for route '{$path}' specified"
            );
        }
        
        $path = preg_replace('#^\/#', '', $path);
        $path = preg_replace('#\/$#', '', $path);

        $data['path'] = $path;
        $data['type'] = $type;
        
        if( empty($data['name']) ) {
            $data['name'] = snake_case($data['controller']) . '.' . snake_case($data['method']);
        }
        
        $className = '\\App\\Controllers\\' . $data['controller'];
        
        if( !controller_exists($data['controller']) ) {
            throw new MicroRouterException(
                "The specified controller {$data['controller']}.php does not exist"
            );
        }
        
        if( !class_exists($className) ) {
            require(ACTIONS_DIR . DIR_SEP . "{$data['controller']}.php");
            
            if( !class_exists($className) ) {
                throw new MicroRouterException(
                    'Incorrect comtroller class name'
                );
            }
        }

        $obj = new $className();

        if( !method_exists($obj, $data['method']) ) {
            throw new MicroRouterException(
                "The specified controller is missing a '{$data['method']}' method"
            );
        }
        
        foreach($this->_patterns as $key=>$pattern) {
            $path = str_replace("\{{$key}\}", "({$pattern})", $path);
            $path = str_replace("\{{$key}?\}", "\/?({$pattern})?", $path);
        }
        
        $regexp = preg_replace('#(\{[0-9a-zA-Z]+\})#is', '([0-9a-zA-Z\-\_]+)', $path);
        $regexp = preg_replace('#(\{[0-9a-zA-Z]+\?\})#is', '\/?([0-9a-zA-Z\-\_]+)?', $regexp);
        
        $data['regexp'] = "^\/{$regexp}$";
        
        $this->_routes[] = $data;

        return $this;
    }
    
    public function pattern($key, $pattern)
    {
        $this->_patterns[ $key ] = $pattern;
        
        return $this;
    }
    
    public function redirect($name, array $values = [])
    {
        
        redirect(
            $this->getPathByName($name, $values)
        );
    }
    
    public function getPathByName($name, array $values = [])
    {
        $path = '';
        
        foreach($this->_routes as $route) {
            if( $route['name'] == $name && ($route['type'] == 'GET' || $route['type'] == 'ANY') ) {
                $path = $route['path'];
                break;
            }
        }
        
        if( empty($path) ) {
            throw new MicroRouterException(
                "Invalid route name `{$name}`"
            );
        }
        
        if( count($values) > 0 ) {
            foreach($values as $key=>$val) {
                $path = str_replace(["\{{$key}\}", "\{{$key}?\}"], $val, $path);
            }
        }
        
        return $path;
    }
}