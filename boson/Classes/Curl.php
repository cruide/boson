<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

  class Curl
  {
      protected
          $_auth_need,
          $_auth_login,
          $_auth_password,

          $_proxy_auth_need,
          $_proxy_auth_login,
          $_proxy_auth_password,
          
          $_post_data,

          $_session,
          $_options,
          $_result,
          $_url;
          
      protected
          $useragent,
          $timeout;
// -------------------------------------------------------------------------------------
      /**
      * Constructor of class
      */
      public function __construct()
      {
          if( !function_exists('curl_init') ) {
              abort('cURL Class - PHP was not built with --with-curl, rebuild PHP to use cURL.');
          }

          $this->timeout    = 30;
          $this->_options   = [];
          $this->useragent  = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1';
          $this->_auth_need = false;
      }
// -------------------------------------------------------------------------------------
      /**
      * Set timeout for request
      * 
      * @param integer $timeout
      * @return Curl
      */
      public function setTimeout( $timeout = 30 )
      {
          if( is_integer( $timeout ) ) {
              $this->timeout = $timeout;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Initialize curl
      */
      protected function _init()
      {
          if( !preg_match('!^\w+://! i', $this->_url) ) {
              $this->_url = 'http://' . $this->_url;
          }
          
          $this->_session = curl_init( (!empty($this->_url)) ? $this->_url : '' );
          
          if( preg_match('/^https/i', $this->_url) ) {
              curl_setopt($this->_session, CURLOPT_SSL_VERIFYPEER, false);
          }
          
//          curl_setopt($this->_session, CURLOPT_REFERER, BASE_URL);
          curl_setopt($this->_session, CURLOPT_USERAGENT     , (!empty($this->useragent)) ? $this->useragent : 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1' );
          curl_setopt($this->_session, CURLOPT_TIMEOUT       , $this->timeout);
          curl_setopt($this->_session, CURLOPT_RETURNTRANSFER, true);
          //curl_setopt($this->_session, CURLOPT_HTTPHEADER    , ['Expect:']); 
          
          if( $this->_auth_need ) {
              curl_setopt($this->_session, CURLOPT_USERPWD, $this->_auth_login . ':' . $this->_auth_password);
          }
          
          if( !empty($this->_options) ) {
              foreach($this->_options as $key=>$val) {
                  curl_setopt($this->_session, $key, $val);
              }
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Disable http authorization
      */
      public function disableHttpAuth()
      {
          $this->_auth_need = false;
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Enable http authorization
      */
      public function enableHttpAuth()
      {
          if( !empty($this->_auth_login) && !empty($this->_auth_password) ) {
              $this->_auth_need = false;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Set authorization user
      * 
      * @param string $username
      * @param string $password
      * @return Curl
      */
      public function setHttpAuth($username = '', $password = '')
      {
          if( !empty($username) && !empty($password) ) {
              $this->_auth_login    = $username;
              $this->_auth_password = $password;
              $this->_auth_need     = true;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Set option for curl
      * 
      * @param string $name
      * @param mixed $value
      * @return Curl
      */
      public function option($name, $value = null)
      {
          if( is_array($name) ) {
              foreach($name as $key=>$val) {
                  $this->_options[ $key ] = $val;
              }
              
          } else {
              if( !empty($name) && !empty($value) ) {
                  $this->_options[ $name ] = $value;
              }
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Execute request
      */
      public function execute()
      {
          $this->_init();
          $this->_result = curl_exec( $this->_session );
          
          if( curl_errno($this->_session) ) { 
          	  return curl_error($this->_session);
          }
          
          if( !empty($this->_result) ) { 
          	  return $this->_result;
          }
          
          return null;
      }
// -------------------------------------------------------------------------------------
      /**
      * Set proxy for request
      * 
      * @param string $url
      * @param integer $port
      * @return Curl
      */
      public function proxy($url, $port = 8080)
      {
          if( !empty($url) && is_numeric($port) ) {
              $this->option(CURLOPT_HTTPPROXYTUNNEL, true);
              $this->option(CURLOPT_PROXY, "{$url}:80");
          }
        
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Set authorization data for proxy
      * 
      * @param string $username
      * @param string $password
      * @return Curl
      */
      public function proxyLogin($username = '', $password = '')
      {
          if( !empty($username) && !empty($password) ) {
              $this->option(CURLOPT_PROXYUSERPWD, "{$username}:{$password}");
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * GET request
      * 
      * @param string $url
      */
      public function get($url = '')
      {
          if( !empty($url) ) {
              $this->_url = $url;
          }
          
          if( !empty($this->_url) ) {
              
              if( isset($this->_options[ CURLOPT_POST ]) ) {
                  unset(
                      $this->_options[ CURLOPT_POST ], 
                      $this->_options[ CURLOPT_POSTFIELDS ]
                  );
              }
              
              $this->_options[ CURLOPT_RETURNTRANSFER ] = true;

              return $this->execute();
          }
          
          return null;
      }
// -------------------------------------------------------------------------------------
      /**
      * POST request
      * 
      * @param string $url
      * @param array $params
      */
      public function post($url, $params = [])
      {
          if( !empty($params) && is_array($params) ) {
              $params = http_build_query($params);
          }
          
          if( empty($url) ) {
              return null;
          }
          
          $this->_url = $url;

          unset( $this->_options[ CURLOPT_RETURNTRANSFER ] );
          
          $this->_options[ CURLOPT_POST ]       = true;
          $this->_options[ CURLOPT_POSTFIELDS ] = $params;

          if( !empty($this->_url) ) {
              return $this->execute();
          }
          
          return null;
      }
// -------------------------------------------------------------------------------------
      /**
      * JSON request
      * 
      * @param string $url
      * @param array $params
      * @param bool $assoc
      */
      public function json($url, $params = [], $assoc = null)
      {
          if( !empty($params) && is_array($params) ) {
              $params = json_encode($params);
          }
          
          if( !empty($url) ) {
              $this->_url = $url;
          }

          $this->_options[ CURLOPT_POST ]           = true;
          $this->_options[ CURLOPT_POSTFIELDS ]     = $params;
          $this->_options[ CURLOPT_CUSTOMREQUEST ]  = 'POST';
          $this->_options[ CURLOPT_RETURNTRANSFER ] = true;
          $this->_options[ CURLOPT_HTTPHEADER ]     = [
              'Content-Type: application/json',
              'Content-Length: ' . strlen($params),
          ];

          if( !empty($this->_url) && ($_ = $this->execute()) ) {
              return json_decode( $_, $assoc );
          }
          
          return null;
      }
// -------------------------------------------------------------------------------------
      /**
      * Set cookie for request
      * 
      * @param array $params
      * @return Curl
      */
      public function setCookie( $params = [] ) 
      {
          if( !empty($params) ) {
              if( is_array($params) ) {
                  $params = http_build_query($params);
              }

              $this->option(CURLOPT_COOKIE, $params);
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      /**
      * Return CURL info
      * 
      */
      public function getInfo()
      {
		  if( empty($url) ) {
			  return null;
		  }
		  
		  return curl_getinfo($this->_session);
      }
// -------------------------------------------------------------------------------------
      function __destruct()
      {
          if( !empty($this->_session) ) {
              curl_close($this->_session);
          }
      }
// -------------------------------------------------------------------------------------
      public function nossl()
      {
          $this->option(CURLOPT_SSL_VERIFYHOST, false);
          $this->option(CURLOPT_SSL_VERIFYPEER, false);
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public static function make($options = [])
      {
          $_ = new self();

          if( is_array($options) && count($options) > 0 ) {
              $_->option($options);
          }
          
          return $_;
      }
  }
