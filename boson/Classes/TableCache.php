<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

class TableCacheException extends \Exception {};

class TableCache
{
    protected static $table = 'cache';
    protected static $key_has_cache = [];
    
    public static function check()
    {
        if( hasTable(self::$table) ) {
            $timestamp = time();
            $items     = table(self::$table)->where('expiration', '<', $timestamp)->get();
            
            if( array_count($items) > 0 )  {
                foreach($items as $item) {
                    if( !empty(self::$key_has_cache[$item->key]) ) {
                        unset( self::$key_has_cache[$item->key] );
                    }
                }
                
                table(self::$table)->where('expiration', '<', $timestamp)->delete();
            }
        }
    }
    
    public static function has($key)
    {
        if( !hasTable(self::$table) ) {
            throw new TableCacheException(
                'TableCache table not created. Use TableCache::install() for creating cache table.'
            );
        }
        
        if( !is_array(self::$key_has_cache) ) {
            self::$key_has_cache = [];
        }
        
        if( !empty(self::$key_has_cache[$key]) ) {
            return true;
        }
        
        $item = table(self::$table)->where('key', '=', $key)
                                   ->first();
                       
        if( $item ) {
            self::$key_has_cache[ $key ] = $item;

            return true;
        }
                                   
        return false;
    }
    
    public static function get($key)
    {
        if( self::has($key) ) {
            return unserialize(
                self::$key_has_cache[ $key ]->value
            );
        }
        
        return null;
    }

    public static function put($key, $value, $expire = UNIXTIME_HOUR)
    {
        if( self::has($key) ) {
            table(self::$table)->where('key', '=', $key)->delete();
            unset( self::$key_has_cache[$key] );
        }
        
        try {
            if( is_callable($value) ) {
                $value = $value();
            }
            
            table(self::$table)->insert([
                'key'        => $key,
                'value'      => serialize($value),
                'expiration' => time() + $expire,
            ]);
        } catch( TableCacheException $e ) {
            throw $e;
        }
    }

    public static function remember($key, \Closure $callback, $expire = UNIXTIME_HOUR)
    {
        if( self::has($key) ) {
            return self::get($key);
        }
        
        $_ = $callback();
        
        self::put($key, $_, $expire);
    }
    
    public static function pull($key)
    {
        if( self::has($key) ) {
            $item = self::$key_has_cache[ $key ];
            
            table(self::$table)->where('key', '=', $key)->delete();
            unset( self::$key_has_cache[$key] );
            
            return $item;
        }
        
        return null;
    }
    
    public static function forget($key)
    {
        if( self::has($key) ) {
            table(self::$table)->where('key', '=', $key)->delete();
            unset( self::$key_has_cache[$key] );
        }
    }
    
    public static function flush()
    {
        if( hasTable(self::$table) ) {
            $table = dbCfg('prefix') . self::$table;
            
            dbQuery("TRUNCATE `{$table}`;");
        }
    }
    
    public static function install()
    {
        if( !hasTable(self::$table) ) {
            $table = dbCfg('prefix') . self::$table;
            
            dbQuery("
                CREATE TABLE IF NOT EXISTS `{$table}` (
                  `id` bigint(20) unsigned NOT NULL DEFAULT '0',
                  `key` varchar(255) NOT NULL DEFAULT '',
                  `value` mediumtext,
                  `expiration` bigint(20) unsigned NOT NULL DEFAULT '0',
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `key` (`key`),
                  KEY `expiration` (`expiration`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
            ");
            
            hasTable(self::$table, true);
        }
    }
}