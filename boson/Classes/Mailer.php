<?php namespace App\Library;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

class Mailer
{
    protected 
        $phpmail, 
        $settings, 
        $ui;

    /**
    * Constructor of class
    */
    public function __construct()
    {
        $mails_path = APP_DIR . DIR_SEP . 'mails';
        
        if( !is_dir($mails_path) ) {
            @mkdir($mails_path, 0777, true);
        }
        
        $this->settings = cfg('mailer');
        $this->phpmail  = new \PHPMailer();
        $this->ui       = new \Smarty();

        $this->ui->setTemplateDir( $mails_path );
        $this->ui->setCompileDir( SMARTY_TEMP_DIR . DIR_SEP . 'mails' );
        $this->ui->setCacheDir( SMARTY_TEMP_DIR . DIR_SEP . 'mails' . DIR_SEP . 'cache' );
	
        if( !empty($this->settings->email_from) ) {
            $this->phpmail->SetFrom(
                $this->settings->email_from,
                (isset($this->settings->from_name)) ?
                    $this->settings->from_name : ''
            );
        } else {
            throw new \Exception('Config ' . SETTINGS_DIR . DIR_SEP . 'mailer.ini not found.');
        }

        if( !empty($this->settings->replyto) ) {
            $this->phpmail->AddReplyTo( $this->settings->replyto );
        }

        if( !empty($this->settings->host) ) {
            $this->phpmail->Host = $this->settings->host;
        }
        
        if( !empty($this->settings->port) ) {
            $this->phpmail->Port = $this->settings->port;
        }
        
        if( !empty($this->settings->username) ) {
            $this->phpmail->Username = $this->settings->username;

            if( !empty($this->settings->password) ) {
                $this->phpmail->Password = $this->settings->password;
                $this->phpmail->SMTPAuth = true;
            }
        }
        
        if( !empty($this->settings->authtype) ) {
            $this->phpmail->AuthType = $this->settings->authtype;
        }
        
        if( !empty($this->settings->type) ) {
            switch($this->settings->type) {
                case 'sendmail': 
                   if( isset($this->settings->sendmail) ) { 
                       $this->phpmail->Sendmail = (string)$this->settings->sendmail; 
                   }
                   
                   $this->phpmail->IsSendmail(); 
                   break;
                   
                case 'smtp':     $this->phpmail->IsSMTP(); break;
                case 'qmail':    $this->phpmail->IsQmail(); break;
                default:         $this->phpmail->IsMail();
            }
        } else {
            $this->phpmail->IsMail();
        }

        $this->phpmail->CharSet = 'utf-8';
    }

    /**
    * Set sender address
    * 
    * @param string $address
    * @param string $name
    * @param mixed $auto
    * @return Mailer
    */
    public function from($address, $name = '', $auto = true)
    {
        $this->phpmail->SetFrom($address, $name, $auto);
        
        return $this;
    }

    /**
    * Set mail subject
    * 
    * @param string $subject
    * @return Mailer
    */
    public function subject($subject)
    {
        $this->phpmail->Subject = htmlspecialchars( $subject );

        return $this;
    }

    /**
    * Set recipient
    * 
    * @param string $address
    * @param string $name
    * @return Mailer
    */
    public function to($address, $name)
    {
        $this->phpmail->AddAddress($address, $name);
        
        return $this;
    }

    /**
    * Set carbon copy recipient
    * 
    * @param string $address
    * @param string $name
    * @return Mailer
    */
    public function cc($address, $name)
    {
        $this->phpmail->AddCC($address, $name);
        
        return $this;
    }

    public function bcc($address, $name)
    {
        $this->phpmail->AddBCC($address, $name);
        
        return $this;
    }

    /**
    * Attach file to mail
    * 
    * @param mixed $path
    * @param mixed $name
    * @param mixed $encoding
    * @param mixed $type
    * @param mixed $disposition
    * @return Mailer
    */
    public function attach($path, $name = '', $encoding = 'base64', $type = '', $disposition = 'attachment')
    {
        $this->phpmail->AddAttachment($path, $name, $encoding, $type, $disposition);
        
        return $this;
    }

    public function assign($tpl_var, $value = null, $nocache = false)
    {
        $this->ui->assign($tpl_var, $value, $nocache);
        return $this;
    }

    public function fetch($template)
    {
        if( !preg_match('#\.tpl$#', $template) ) {
            $template .= '.tpl';
        }

        return $this->ui->fetch($template);
    }

    /**
    * Send mail
    * 
    * @param string $template
    */
    public function send($template, $values = null)
    {
        if( !preg_match('#\.tpl$#', $template) ) {
            $template .= '.tpl';
        }

        if( !empty($values) && is_array($values) ) {
            $this->ui->assign($values);
        }

        $this->phpmail->MsgHTML(
            $this->ui->fetch($template)
        );

        try {
            $_ = $this->phpmail->Send();
            
        } catch( \Exception $e ) {
            abort( $e->getMessage() );
        }
        
        return $_;
    }

    public function sendHTML( $html )
    {
        $this->phpmail->MsgHTML( $html );

        try {
            $_ = $this->phpmail->Send();
            
        } catch( \Exception $e ) {
            abort( $e->getMessage() );
        }
        
        return $_;
    }

    public function __call($name, $args)
    {
        if( method_exists($this->phpmail, $name) ) {
            return call_user_func_array(
                array($this->phpmail, $name),
                $args
            );
        } else {
            throw new \Exception(
                "Call to undefined method {$name}"
            );
        }
    }
}
