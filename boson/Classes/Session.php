<?php namespace Boson;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

use Boson\Traits\SingletonTrait;

class Session
{
    use SingletonTrait;
    
    protected $_session_id;

    public function __construct()
    {
        session_start();
        $this->_session_id = session_id();
    }

    public function id( $crypt = false )
    {
        return $crypt ? md5($this->_session_id) : $this->_session_id;
    }

    public function destroy()
    {
        session_destroy();
    }
    
    public function has($key)
    {
        global $_SESSION;

        return array_key_isset($key, $_SESSION);
    }
    
    /**
    * Cookie getter for $this->_properties
    *
    * @param strung $name
    */
    public function __get($name)
    {
        global $_SESSION;

        if( array_key_isset($name, $_SESSION) ) {
            return $_SESSION[ $name ];
        }

        return null;
    }

    /**
    * Cookie setter for $this->_properties
    *
    * @param string $name
    * @param mixed $value
    */
    public function __set($name, $value)
    {
        global $_SESSION;

        $_SESSION[ $name ] = $value;
    }

    /**
    * Cookie isset for $this->_properties
    *
    * @param string $name
    * @return bool
    */
    public function __isset($name)
    {
        global $_SESSION;

        return isset($_SESSION[ $name ]);
    }

    /**
    * Cookie unset for $this->_properties
    *
    * @param string $name
    */
    public function __unset($name)
    {
        global $_SESSION;

        unset($_SESSION[ $name ]);
    }
}


