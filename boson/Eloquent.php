<?php
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

/*
use \Illuminate\Database\Capsule\Manager as DB;
use \Illuminate\Database\Schema\Builder as Schema;
*/

use \Illuminate\Database\Schema\Builder as Schema;

$EloquentORM = new \Illuminate\Database\Capsule\Manager();
$EloquentINI = cfg('database');

if( !empty($EloquentINI->host) ) {
    $EloquentORM->addConnection([
        'driver'    => !empty($EloquentINI->driver) ? $EloquentINI->driver : 'mysql',
        'host'      => $EloquentINI->host,
        'database'  => $EloquentINI->database,
        'username'  => $EloquentINI->username,
        'password'  => $EloquentINI->password,
        'charset'   => !empty($EloquentINI->charset) ? $EloquentINI->charset : 'utf8',
        'collation' => !empty($EloquentINI->collation) ? $EloquentINI->collation : 'utf8_general_ci',
        'prefix'    => $EloquentINI->prefix,
    ], 'default');
    
    $EloquentORM->setEventDispatcher(
        new \Illuminate\Events\Dispatcher(
            new \Illuminate\Container\Container()
        )
    );
    
    $EloquentORM->setAsGlobal();
    $EloquentORM->bootEloquent();
}

class_alias('\\Illuminate\\Database\\Capsule\\Manager', '\\DB');

unset($EloquentINI);

function table($table): \Illuminate\Database\Query\Builder
{
    return \DB::table($table);
}

function hasTable($tablename, $refresh = false)
{
    static $tables;

    $prefix   = \DB::getTablePrefix();
    $database = \DB::getDatabaseName();
    
    if( empty($tables) || $refresh ) {
        $tables = [];
        
        $field_name = "Tables_in_{$database}";
        $data       = \DB::select( \DB::raw("SHOW TABLES FROM {$database}") );
        
        foreach($data as $item) {
            $tables[] = str_replace($prefix, '', $item->$field_name);
        }
    }
    
    return in_array($tablename, $tables);
}

function getTableFieldsList($tablename)
{
    static $fileds;
    
    $prefix = \DB::getTablePrefix();
    
    if( empty($fileds) || !array_key_isset($tablename, $fileds) ) {
        if( !is_array($fileds) ) {
            $fileds = [];
        }
        
        $_     = [];
        $items = \DB::select( \DB::raw("SHOW FIELDS FROM {$prefix}{$tablename}") );
        
        foreach($items as $item) {
            $_[] = $item->Field;
        }
        
        $fileds[ $tablename ] = $_;
    }
    
    return $fileds[ $tablename ];
}

function getTablePrimaryKey($tablename)
{
    static $keys;

    $prefix = \DB::getTablePrefix();
    
    if( !is_array($keys) ) {
        $keys = [];
    }
    
    if( !array_key_isset($tablename, $keys) ) {
        $keys[ $tablename ] = null;

        $items = \DB::select("SHOW KEYS FROM {$prefix}{$tablename}");
        
        foreach($items as $item) {
            if( $item->Key_name == 'PRIMARY' ) {
                $keys[ $tablename ] = $item->Column_name;
            }
        }
    }
    
    return $keys[ $tablename ];
}

function collection( $mixed = null )
{
    return new \Illuminate\Database\Eloquent\Collection( $mixed );
}

function schemaTable($table, Closure $callback, $connection = null)
{
    global $EloquentORM;
    
    return $EloquentORM->connection($connection)
                       ->getSchemaBuilder()
                       ->table($table, $callback);
}

function schemaTableCreate($table, Closure $callback, $connection = null)
{
    global $EloquentORM;
      
    return $EloquentORM->connection($connection)
                       ->getSchemaBuilder()
                       ->create($table, $callback);
}

function schemaTableDrop($table, $if_exists = false, $connection = null)
{
    global $EloquentORM;
      
    if( $if_exists ) {
        return $EloquentORM->connection($connection)
                           ->getSchemaBuilder()
                           ->dropIfExists($table);
    }

    return $EloquentORM->connection($connection)
                       ->getSchemaBuilder()
                       ->drop($table);
}

function schemaTableRename($table, $to, $connection = null)
{
    global $EloquentORM;
      
    return $EloquentORM->connection($connection)
                       ->getSchemaBuilder()
                       ->rename($table, $to);
} 
  




  