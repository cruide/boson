<?php namespace Boson\Abstracts;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2019 All rights reserved
*/

abstract class EloquentModel extends \Illuminate\Database\Eloquent\Model
{
    use \Boson\Traits\ClassName;
    
    public function __construct(array $attributes = [], $table = null)
    {
        if( !empty($table) ) {
            $this->table = $table;
        }
        
        parent::__construct($attributes);
    }
    
    public function scopeOrderByRandom(Builder $query)
    {
        return $query->orderByRaw('RANDOM()');
    }

    public function scopeWhereLike(Builder $query, $fieldname, $str)
    {
        return $query->whereRaw("CAST(`{$fieldname}` AS CHAR) LIKE '%{$str}%'");
    }

    public function scopeOrWhereLike(Builder $query, $fieldname, $str)
    {
        return $query->orWhereRaw("CAST(`{$fieldname}` AS CHAR) LIKE '%{$str}%'");
    }

    public function scopeWhereFulltextMatch(Builder $query, $fieldname, $searchtext)
    {
        if( is_array($fieldname) ) {
            $fieldname = implode(',', $fieldname);
        }
        
        return $query->whereRaw("MATCH({$fieldname}) AGAINST(? IN BOOLEAN MODE)", ["*{$searchtext}*"]);
    }
        
    public function scopeOrWhereFulltextMatch(Builder $query, $fieldname, $searchtext)
    {
        if( is_array($fieldname) ) {
            $fieldname = implode(',', $fieldname);
        }
        
        return $query->orWhereRaw("MATCH({$fieldname}) AGAINST(? IN BOOLEAN MODE)", ["*{$searchtext}*"]);
    }

    public function jsonSerialize()
    {
        return method_exists($this, 'transform') ? $this->transform() : $this->toArray(); 
    }
}

