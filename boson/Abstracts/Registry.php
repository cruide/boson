<?php namespace Boson\Abstracts;
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/
  
abstract class Registry extends \stdClass implements \ArrayAccess, \Countable, \IteratorAggregate, \JsonSerializable
{
    use \Boson\Traits\ClassName;
    
	protected $properties = [];
	
	/**
	* Getter for $this->properties
	* 
	* @param strung $name
	*/
	public function __get($name)
	{
		return $this->get($name);
	}
	
	/**
	* Setter for $this->properties
	* 
	* @param string $name
	* @param mixed $value
	*/
	public function __set($name, $value)
	{
		$this->set($name, $value);
	}
	
	/**
	* Isset for $this->properties
	* 
	* @param string $name
	* @return bool
	*/
	public function __isset($name)
	{
		return isset($this->properties[$name]);
	}
	
	/**
	* Unset for $this->properties
	* 
	* @param string $name
	*/
	public function __unset($name)
	{
		unset($this->properties[$name]);
	}
    
    public function __toString()
    {
        return $this->toJson();
    }
    
    public function has( $name )
    {
        return ( !empty($name) && array_key_isset($name, $this->properties) );
    }
    
    public function set($name, $value = null)
    {
        $mutator_name = 'set' . studly_case($name) . 'Attribute';
        
        if( method_exists($this, $mutator_name) ) {
            $this->properties[ $name ] = $this->$mutator_name($value);
            
        } else if( array_key_isset($name, $this->properties) && $value === null ) {
            unset( $this->properties[$name] );
            
        } else {
            $this->properties[ $name ] = $value;
        }
    }
    
    public function get( $name )
    {
        $accessor_name = 'get' . studly_case($name) . 'Attribute';

        if( method_exists($this, $accessor_name) ) {
            return $this->$accessor_name();
        }

        if( array_key_isset($name, $this->properties) ) {
            return $this->properties[ $name ];
        }
        
        return null;
    }
    
    public function count()
    {
        return count( $this->properties );
    }
    
    public function serialize()
    {
        $_ = [];
        
        foreach($this->properties as $key=>$val) {
            $_[ $key ] = $this->get($key);
        }

        return serialize($_);
    }
    
    public function toJson()
    {
        $_ = $this->toArray();

        if( method_exists($this, 'jsonTransform') ) {
            return $this->jsonTransform( $_ );
        }
        
        return $_;
    }
    
    public function toArray()
    {
        $_ = [];
        
        foreach($this->properties as $key=>$val) {
            if( is_object($val) && method_exists($val, 'toArray') ) {
                $_[ $key ] = $val->toArray();
            } else {
                $_[ $key ] = $this->get($key);
            }
        }

        return $_;
    }
    
    public function fill( $properties )
    {
        if( array_count($properties) > 0 ) {
            foreach($properties as $key=>$val) {
                if( is_varible_name($key) ) {
                    $this->properties[ $key ] = $val;
                }
            }
        }
        
        return $this;
    }
    
    /**
     * Determine if an item exists at an offset.
     *
     * @param  mixed  $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return array_key_isset($key, $this->properties);
    }

    /**
     * Get an item at a given offset.
     *
     * @param  mixed  $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * Set the item at a given offset.
     *
     * @param  mixed  $key
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($key, $value)
    {
        if( is_null($key) ) {
            $this->properties[] = $value;
            
        } else {
            $this->set($key, $value);
        }
    }

    /**
     * Unset the item at a given offset.
     *
     * @param  string  $key
     * @return void
     */
    public function offsetUnset($key)
    {
        unset($this->properties[$key]);
    }
    
    /**
     * Get an iterator for the items.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator( $this->toArray() );
    }
    
    public function jsonSerialize()
    {
        return $this->toJson();
    }
}



