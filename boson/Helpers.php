<?php
/**
* @name      Boson PHP framework
* @author    Tishchenko Alexander (info@alex-tisch.ru)
* @copyright Copyright (c) 2018 All rights reserved
*/

// -----------------------------------------------------------------------------
  /**
  * Aliases
  */
  function input()
  {
	  return \Boson\Input::getInstance();
  }
// -----------------------------------------------------------------------------
  function session($key = null)
  {
	  if( !empty($key) ) {
		  return \Boson\Session::getInstance()->$key;
	  }
	  
	  return \Boson\Session::getInstance();
  }
// -----------------------------------------------------------------------------
  function cookies($key = null)
  {
	  if( !empty($key) ) {
		  return \Boson\Cookies::getInstance()->$key;
	  }
	  
	  return \Boson\Cookies::getInstance();
  }
// -----------------------------------------------------------------------------
  function router()
  {
	  return \Boson\MicroRouter::getInstance();
  }
// -----------------------------------------------------------------------------
  function app()
  {
	  return \Boson\App::getInstance();
  }
// -----------------------------------------------------------------------------
  function validator(array $values, array $rules)
  {
	  return new \Boson\Validator($values, $rules);
  }
// -----------------------------------------------------------------------------
  function i18n($key = null, array $values = [])
  {
	  if( !empty($key) ) {
		  return \Boson\I18n::getInstance()->get($key, $values);
	  }
	  
	  return \Boson\I18n::getInstance();
  }
// -----------------------------------------------------------------------------
  function dbCfg($key = null)
  {
	  return cfg('database', $key);
  }
// -----------------------------------------------------------------------------
	  
  function curl($options = [])
  {
	  return \Boson\Curl::make($options);
  }

// -----------------------------------------------------------------------------
  function cache($key, $value = null, $expire = UNIXTIME_HOUR)
  {
	  if( !empty($key) && $value !== null ) {
		  return \Boson\TableCache::put($key, $value, $expire);
	  }
	  
	  return \Boson\TableCache::pull($key);
  }
// -----------------------------------------------------------------------------
  function cacheRemember($key, \Closure $callback, $expire = UNIXTIME_HOUR)
  {
	  return \Boson\TableCache::remember($key, $callback, $expire);
  }
// -----------------------------------------------------------------------------
  function smarty_function_i18n($params, &$smarty)
  {
	  if( !empty($params['str']) ) {
		  if( !empty($params['mod']) && function_exists("str_{$params['mod']}") ) {
			  $modificator = "str_{$params['mod']}";
			  
			  return $modificator( i18n($params['str']) );
			  
		  } else if( !empty($params['mod']) && function_exists($params['mod']) ) {
			  $modificator = $params['mod'];
			  
			  return $modificator( i18n($params['str']) );
		  }
		  
		  return i18n($params['str']);
	  }
	  
	  return '';
  }
// -----------------------------------------------------------------------------
  function smarty_function_num2word($params, &$smarty)
  {
	  if( !array_key_isset('number', $params) || empty($params['words']) || !is_array($params['words']) ) {
		  return '%num2word_error%';
	  }
	  
	  return num2word($params['number'], $params['words']);
  }
  