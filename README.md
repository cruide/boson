# BOSON PHP7 MVC micro-framework.
Маленький и быстрый PHP7 MVC micro-framework.

Вам необходимо написать простое веб-приложение легко и быстро?

Нет ничего проще! BOSON для этого и создан!

- Минимум настроек;
- MVC структура;
- Понятный и простой код;
- Удобный интерфейс для работы с Базой Данных;
- Поддержка мульти-язычности

## База данных

Для работы с базой данных используется [Laravel Eloquent ORM](https://github.com/LaravelRUS/docs/blob/6.x/eloquent.md), что делает работу с базой данных простой и удобной.

## Шаблонизатор Smarty

Для проекта, в качестве шаблонизатора используется [Smarty template engine](http://smarty.net)
или нативный PHTML


## Структура приложения
### Директории:

- app
    - controllers
        - Index.php
    - library
    - lang
        - en.php
        - ru.php
    - models
        - MyModel.php
    - configs
        - config.ini
        - database.ini
    - Bootstrap.php
    - Routes.php
    - composer.json
- content
- themes
    - bootstrap
        - css
        - js
        - images
        - views
            - index
                - index.tpl
            - layout.tpl
- tmp
- .htaccess
- index.php

### Роутинг
Система роутинга упрощена до минимума с учетом возможности задавать свои варианты роутов.

```PHP
<?php

router()->pattern('id', '[0-9]+');
router()->get('/user/{id}', 'Users@get');

router()->get([
    ['/', 'Index@index'],
    ['/register', 'Index@register'],
    ['/wot', 'Index@wot'],
    ['/install', 'Install@index'],
]);
        
router()->any('/login', 'Index@login', 'index.login')
        ->any('/logout', 'Index@logout', 'index.logout')
        ->any('/register', 'Index@register', 'index.register');

router()->resource('/photo', 'Photo');

```

Получить id можно двумя способами:

```PHP
<?php

$id = input()->get('id'); /* это равносильно $id = input()->id; */

/* или в качестве параметра в методе контроллера */

public function get($id)
{

}

```

### Контроллер

Пример контроллера:
```PHP
<?php namespace App\Controllers;

class Index
{
    /* Будет запущен перед вызываемым методом контроллера */
    public function _before()
    {
        
    }
    
    /* Будет запущен после вызываемого метода контроллера */
    public function _after()
    {
        
    }

    public function index()
    {
        return view('index');
    }

    public function get( $user_id = null )
    {
        return json_response([
            'status' => 'success',
            'user'   => user( $user_id ),
        ]);
    }
}

```

## Работа с базой данных

Использование Laravel Eloquent ORM сделает работу с приложением очень гибкой и удобной.

### Пример модели
```PHP
<?php namespace App\Models;

class User extends \Boson\Abstracts\EloquentModel
{
    public function profile()
    {
        return $this->hasOne( \App\Models\Profile::class );
    }
}
```

## P.S.

Прошу не судить строго. Проект в начале своего пути и постоянно дорабатывается.

## Автор
Тищенко Александр / info@alex-tisch.ru